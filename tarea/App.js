import React, { Component } from 'react';
import { StyleSheet, Text, View , Image , TouchableOpacity, TextInput} from 'react-native';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      textValue: '',
      count : 0,
    };
  }

  changeTextInput = text =>{
    this.setState({textValue: text});
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text>Pantalla de login</Text>
        </View>

        <Image style={styles.image} source={require('./img/login_logo.png')} />
        <View style={styles.text}>
          <Text>Correo o usuario</Text>
        </View>
        <TextInput
          style={{height: 40, borderColor: 'gray' , borderWidth: 1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />
        <View style={styles.text}>
          <Text>Contraseña</Text>
        </View>
        <TextInput
          style={{height: 40, borderColor: 'gray' , borderWidth: 1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />
        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text>Ingresar</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal:10,
  },
  text:{
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  image: {
    height:300,
    width:300,
    alignSelf: 'center',
  },
});
